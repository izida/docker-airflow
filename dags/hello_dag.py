from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago

default_args = {
    'owner': 'Nick',  # Владелец
    # формальная дата начала задачи
    'start_date': days_ago(0),  # days_ago(0), т.е. равное текущему дню
    'depends_on_past': False,  # Для запуска графа задним числом
    # Список адрессов куда будут отрпалены результаты выполнения
    # 'email': ['example@biconsult.ru'],
    # Если True, то при неудачном завершении задачи будет отправлено
    # уведомление по электронной почте.
    # 'email_on_failure': True,
    # 'email_on_retry': True,  # Будет отправлено уведомление о презапуске
    # 'retries': 5,  # Количество попыток перезапуска задачи
    # 'retry_delay': 5,  # Интервал времени между перезапусками
    # 'queue': ?,  # Пул задач, к которому принадлежит задача.
    # 'pool' :? , #  Набор ресурсов, которые могут использоваться задачей.
    # 'priority_weight': 1, # Задачи с большим весом имеют больший приоритет.
    # 'end_date': datetime(2024, 5, 24)  # Время окнчания Дага
    # 'wait_for_downstream': True  # Задача не начнётся пока другие даги не выполнятся  # noqa
    # 'sla': timedelta(hours=2)  # Время за которое задача должна выполнится
    # 'execution_timeout': timedelta(seconds=300)  # Если задача не выолненелась за 5 минут(300 сек.) она отменится  # noqa
    # 'on_failure_callback': <name_function>  # Функция, вызыываемая при неудачном завершении задачи # noqa
    # 'on_success_callback': <name_function>  # Функция, при успешном завершении # noqa
    # 'on_retry_callback': <name_function>  # Функция, вызываемая при перезапуске задачи # noqa
    # 'sla_miss_callback': <name_function>  # Функция, которая будет вызвана,
                                            # если задача не была выполнена в пределах SLA. # noqa
    # 'trigger_rule': 'all_success'  # Правило запуска задачи
    # Например, all_success означает,
    # что задача будет запущена только
    # если все предшествующие ей задачи завершились успешно
}


with DAG(
    'mydag',  # Наиминование дага
    default_args=args,
    schedule_interval='@once',  # Запуститься лишь один раз
    catchup=False  # Если будет True то граф запуститься
                   # столько раз сколько ему нужно для
                   # того что бы нагнать текущую дату
) as dag:
    t1 = BashOperator(
        task_id='echo_hi',
        bash_command='echo "Hello"'
    )

    t2 = BashOperator(
        task_id='print_date',
        bash_command='date'
    )

t1 >> t2
